CodePipeline Lambda Function for Integration with Checkmarx
-Automated project creation / update
-Scan Execution

Credit to the following repository for base REST API implementation for Checkmarx. 
https://github.com/binqsoft/CxRestPy.git


aws cloudformation create-stack --stack-name cx-lambda \
--template-body file://cx-lambda-ssm.yml \
--capabilities CAPABILITY_IAM \
--parameters  ParameterKey=CxUrl,ParameterValue="https://cx.xxxxxx.com"  \
ParameterKey=CxUser,ParameterValue=XXXX \
ParameterKey=CxPassword,ParameterValue=XXXX

aws ssm put-parameter --name /Checkmarx/checkmarxURL --type String --value "https://cx.xxxxx.com"
aws ssm put-parameter --name /Checkmarx/chekmarxUser --type SecureString --value "xxxxxx"
aws ssm put-parameter --name /Checkmarx/checkmarxPassword --type SecureString --value "xxxxxx"

{"project" : "lambda"}
